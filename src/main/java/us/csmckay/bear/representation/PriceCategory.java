package us.csmckay.bear.representation;

public enum PriceCategory {
    OPEN, CLOSE, LOW, HIGH, VOLUME, ALL
}
